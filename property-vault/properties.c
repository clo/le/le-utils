// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "properties.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>
#include <log.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <errno.h>

static int open_prop_socket()
{
    int fd, ret = 0;

    fd = socket(PF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0);
    if (fd < 0) {
        ret = -errno;
    } else {
        struct sockaddr_un un;
        memset(&un, 0, sizeof(struct sockaddr_un));
        un.sun_family = AF_UNIX;
        snprintf(un.sun_path, sizeof(un.sun_path),
                        SOCKET_DIR"/%s", PROP_SOCKET_NAME);

        if (connect(fd, (struct sockaddr *)&un, sizeof(struct sockaddr_un)) < 0) {
            ALOGE("Failed to connect to %s socket", PROP_SOCKET_NAME);
            close(fd);
            ret = -errno;
        } else {
        ret = fd;
        }
    }
    return ret;
}

int property_get(const char *key, char *value, const char *default_value) {
    int rc = 0;

    // build msg for socket
    const char msg[MAX_ALLOWED_LINE_LEN+1]; // +1 for msg type.
    char resp[MAX_ALLOWED_LINE_LEN];
    bool is_key_valid = true;

    memset(msg,  0 , sizeof(msg));
    memset(resp, 0 , sizeof(resp));

    snprintf(msg, sizeof msg, "%c%s=",
            PROP_MSG_GETPROP, key);

    int ret = 0;

    // send msg to socket
    int fd = open_prop_socket();
    if (fd < 0) {
       ALOGE("Failed to open Socket");
       is_key_valid = false;
       return fd; // pass error back to caller.
    }

    if (is_key_valid) {

        int msg_len = strlen(msg);
        const int num_bytes = send(fd, msg, msg_len, 0);

        if (num_bytes == msg_len) {
            //Handle data sent from service
            char recv_buf[MAX_ALLOWED_LINE_LEN+1];
            memset(recv_buf, 0 , sizeof(recv_buf));
            int nbytes = recv(fd, recv_buf, sizeof(recv_buf), 0);
            if (nbytes <= 0) {
                ALOGE("recv failed (%s)",strerror(errno));
                ret = -1;
            } else {
                recv_buf[nbytes] = '\0';
                LOG("Received %d bytes of data (%s) from Socket",nbytes, recv_buf);
                int i = 1;
                while(i < strlen(recv_buf)){
                    resp[i-1] = recv_buf[i];
                    i++;
                }
                ret = 0;
            }
        } else {
            ALOGE("%s: Failed to send the msg : num_bytes=%d msg=%s msg_len=%d\n",__func__,num_bytes,msg,msg_len);
            ret = -1;
        }
        close(fd);

        if (ret < 0) {
            LOG("Failed to send message to Get %s", key);
            is_key_valid = false;
        }

        // Extract prop value from response.
        char *delimiter = strchr(resp, '=');
        const char *curr_line_ptr = delimiter+1; //+1 for delimiter
        if(strlen(curr_line_ptr) <= 0) {
            LOG("%s has invalid length", key);
            is_key_valid = false;
        }

        strlcpy(value, curr_line_ptr, PROP_VALUE_MAX);
    }

    if(is_key_valid) rc = strlen(value);
    if( rc > 0) return rc;

    if (NULL != default_value) {
        rc = snprintf(value, PROP_VALUE_MAX, "%.*s", PROP_VALUE_MAX - 1, default_value);
    }

    return rc;
}

int property_set(const char *key, const char *value)
{
    char prop_name[PROP_NAME_MAX];
    char prop_value[PROP_VALUE_MAX];

    if (key == 0) return -1;
    if (value == 0) value = "";
    if (strlen(key) >= PROP_NAME_MAX) return -1;
    if (strlen(value) >= PROP_VALUE_MAX) return -1;

    memset(prop_name, 0, sizeof prop_name);
    memset(prop_value, 0, sizeof prop_value);

    strlcpy(prop_name, key, sizeof prop_name);
    strlcpy(prop_value, value, sizeof prop_value);

    // build msg for socket
    const char msg[MAX_ALLOWED_LINE_LEN+1]; // +1 for msg type.
    memset(msg, 0 , sizeof msg);

    snprintf(msg, MAX_ALLOWED_LINE_LEN+1, "%c%s=%s",
             PROP_MSG_SETPROP, prop_name, prop_value);

    // send message to socket
    int fd, ret = 0;

    fd = open_prop_socket();
    if (fd < 0) {
       ALOGE("Failed to open Socket");
       return fd; // pass error back to caller.
    }

    int msg_len = strlen(msg);
    const int num_bytes = send(fd, msg, msg_len, 0);

    if (num_bytes == msg_len) {
        LOG("Sent %d bytes of data (%s) to Socket", num_bytes, msg);
    } else {
        ALOGE("Failed to send data (%s) to Socket", msg);
        ret = -1;
    }
    close(fd);

    if (ret < 0) {
       ALOGE("Failed to send message to Set %s", prop_name);
    }

    return ret;
}
