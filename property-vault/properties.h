// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "properties_def.h"

/* property_get: returns the length of the value which will never be
** greater than PROP_VALUE_MAX - 1 and will always be zero terminated.
** (the length does not include the terminating zero).
**
** If the property read fails or returns an empty value, the default
** value is used (if nonnull).
*/
/**
 * @param key: for property name
 * @param value: for getting the property value from database/persist
 * @param default_value: for getting default value in case of failure
 */
int property_get(const char *key, char *value, const char *default_value);

/* property_set: returns 0 on success, < 0 on failure
*/
/**
 * Set the property value for a given property name
 * @param keyL for property name
 * @param value: for property value
 * @return 0 on success and -1 otherwise
 */
int property_set(const char *key, const char *value);
