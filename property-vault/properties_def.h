// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include <glib.h>
#define strlcpy g_strlcpy
#define strlcat g_strlcat

#define PROP_FILE_DEFAULT_PATH      "/build.prop"
#define PROP_FILE_PERSIST_PATH      "/etc/build.prop"

#define PROP_MSG_GETPROP '0'
#define PROP_MSG_SETPROP '1'
#define PROP_SOCKET_NAME "property-vault.socket"

#undef  LOG_TAG
#define LOG_TAG "property-vault"

#define PROP_NAME_MAX   32
#define PROP_VALUE_MAX  92
#define MAX_ALLOWED_LINE_LEN        (PROP_NAME_MAX+PROP_VALUE_MAX+1)
#define MAX_PROPERTY_ITER (2)
#define MAX_NUM_PROPERTIES          (10)

#define SOCKET_DIR "/tmp/property-vault"
