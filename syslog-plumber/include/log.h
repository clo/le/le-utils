// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include <syslog.h>
#define LOG_TAG "log: "
#define PRI_INFO " I"
#define PRI_WARN " W"
#define PRI_ERROR " E"
#define PRI_DEBUG " D"
#define PRI_VERB " V"

#define LOG_ERROR LOG_ERR

#define ANDROID_LOG_UNKNOWN LOG_NOTICE
#define ANDROID_LOG_DEFAULT LOG_NOTICE
#define ANDROID_LOG_VERBOSE LOG_DEBUG
#define ANDROID_LOG_DEBUG LOG_DEBUG
#define ANDROID_LOG_INFO LOG_INFO
#define ANDROID_LOG_WARN LOG_WARNING
#define ANDROID_LOG_ERROR LOG_ERROR
#define ANDROID_LOG_FATAL LOG_ERROR
#define ANDROID_LOG_SILENT LOG_NOTICE

#define __android_log_write(prio, tag, text) syslog (prio, tag, text)
#define __android_log_print(prio, tag, fmt, arg...) syslog (prio, tag fmt, ##arg)

#define ALOGV(fmt, arg...) syslog (LOG_NOTICE, LOG_TAG fmt, ##arg)
#define ALOGD(fmt, arg...) syslog (LOG_DEBUG, LOG_TAG fmt, ##arg)
#define ALOGI(fmt, arg...) syslog (LOG_INFO, LOG_TAG fmt, ##arg)
#define ALOGW(fmt, arg...) syslog (LOG_WARNING, LOG_TAG fmt, ##arg)
#define ALOGE(fmt, arg...) syslog (LOG_ERROR, LOG_TAG fmt, ##arg)

#undef LOG
#ifdef LOG_DEBUG
  #define LOG(fmt, args...) \
      ALOGD("%s:%d " fmt "\n", __func__, __LINE__, ##args)
#else
  #define LOG(fmt, args...) do {} while(0)
#endif
